//problem does not look good and small screen devices
//solution hide the text links and show mobile menu

//Create a select and append to menu
var $select = $("<select></select>");
$("#menu").append($select);

//cycle over menu links
$("#menu a").each(function(){
  var $anchor = $(this);
//create an option
  var $option = $("<option></option>");
  
  //deal with selected options depending on current page
  if($anchor.parent().hasClass("selected")){
    $option.prop("selected", true);
  }
//options value is the href
  $option.val($anchor.attr("href"));
//options text is the text of the link
  $option.text($anchor.text());
//append option to select location
  $select.append($option);

  
});

//create a button
//var $button = $("<button>Go</button>");
//$("#menu").append($button);
//bind click to button
//$button.click(function(){
 //go to the select location
  //window.location = $select.val();
//});

//bind click to button
$select.change(function(){
 //go to the select location
  window.location = $select.val();
  
});



